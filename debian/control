Source: golang-github-mendersoftware-go-lib-micro
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Andreas Henriksson <andreas@fatal.se>
Section: golang
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-ant0ine-go-json-rest-dev,
               golang-github-gin-gonic-gin-dev,
               golang-github-google-uuid-dev,
               golang-github-pkg-errors-dev,
               golang-github-satori-go.uuid-dev,
               golang-github-spf13-viper-dev,
               golang-github-stretchr-testify-dev,
               golang-gopkg-tomb.v2-dev,
               golang-logrus-dev
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-mendersoftware-go-lib-micro
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-mendersoftware-go-lib-micro.git
Homepage: https://github.com/mendersoftware/go-lib-micro
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/mendersoftware/go-lib-micro

Package: golang-github-mendersoftware-go-lib-micro-dev
Architecture: all
Depends: golang-github-gin-gonic-gin-dev,
         golang-github-google-uuid-dev,
         golang-github-pkg-errors-dev,
         golang-github-satori-go.uuid-dev,
         golang-github-spf13-viper-dev,
         golang-github-stretchr-testify-dev,
         golang-gopkg-tomb.v2-dev,
         golang-logrus-dev,
         ${misc:Depends}
Description: Group of golang packages for developing microservices. (library)
 Mender: go-lib-micro Mender is an open source over-the-air (OTA) software
 updater for embedded Linux devices. Mender comprises a client running
 at the embedded device, as well as a server that manages deployments
 across many devices.
 .
 This package contains the Mender go-lib-micro library, which is part
 of the Mender server. The Mender server is designed as a microservices
 architecture and comprises several repositories.
 .
 The go-lib-micro library is a collection of utilities and middlewares
 shared among microservices in the Mender ecosystem.
